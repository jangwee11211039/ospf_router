/*************************************************************************
	> File Name: read_file_2.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 15时48分12秒
 ************************************************************************/

//稀疏矩阵
//使用接口函数 Matrix(ParseMatrix*,int int)获取值：
//注意：当元素不存在时 返回INF

#include<stdio.h>
#include<stdlib.h>
#define MaxSize 100000
#define INF 65536


typedef int ElemType;
typedef struct{
	int row,clo;
	ElemType ele;
}Elem;

typedef struct{
	Elem data[MaxSize+1];//不用data[0]
	int num_row;//行数
	int num_clo;//列数
	int num_ele;//元素个数
}SparseMatrix;

bool find_elem(SparseMatrix *M,int row,int clo,ElemType *ele);

//接口函数
void inital_sparse_matrix(SparseMatrix *M,int row,int clo);
bool insert_elem(SparseMatrix *M,int row,int clu,ElemType ele);
ElemType Matrix(SparseMatrix *M,int row,int clo);
void print_all_elem(SparseMatrix *M);
void print_all_elem_2(SparseMatrix *M);

void inital_sparse_matrix(SparseMatrix* M,int row,int clo)
{
	M->num_row=row;
	M->num_clo=clo;
	M->num_ele=0;
}

bool insert_elem(SparseMatrix *M,int row,int clo,ElemType ele)
{
	int i,j,p;
	if(M->num_ele>=MaxSize)
	{
		printf("\n Error:no space! \n");
		return false;
	}
	if((row>M->num_row)||(clo>M->num_clo)||(row<1)||(clo<1))
	{
		printf("\n Error:out of bound! \n");
		return false;
	}
	p=1;
	if(M->num_ele==0)
	{
		M->data[p].row=row;
		M->data[p].clo=clo;
		M->data[p].ele=ele;
		M->num_ele++;
		return true;
	}
	
	for(j=1;j<=M->num_ele;++j)
	{
		if((row>=M->data[j].row)&&(clo>=M->data[j].clo))
			p++;
	}
	//printf("\n p:  %d num_ele: %d \n",p,M->num_ele);
	if((row==M->data[j-1].row)&&(clo==M->data[j-1].clo))
	{
		M->data[j-1].ele=ele;
		return true;
	}

	for(i=M->num_ele;i>=p;--i)
	{
		M->data[i+1].row=M->data[i].row;
		M->data[i+1].clo=M->data[i].clo;
		M->data[i+1].ele=M->data[i].ele;
	}

	M->data[p].row=row;
	M->data[p].clo=clo;
	M->data[p].ele=ele;
	M->num_ele++;

	return true;
}

bool find_elem(SparseMatrix *M,int row,int clo,ElemType* ele)
{
	int p;
	for(p=1;p<=M->num_ele;++p)
	{
		if((row==M->data[p].row)&&(clo==M->data[p].clo))
		{
			*ele=M->data[p].ele;
			return true;
		}
	}
	return false;
}

ElemType Matrix(SparseMatrix *M,int row,int clo)
{
	ElemType ele=INF;
	if(find_elem(M,row,clo,&ele))
	{
		return ele;
	}
	return ele;
}

void print_all_elem(SparseMatrix *M)
{
	int row=M->num_row;
	int clo=M->num_clo;
	for(int i=1;i<=row;++i)
	{
		for(int j=1;j<=clo;++j)
		{
			printf("%d \t\t ",Matrix(M,i,j));
		}
		printf("\n");
	}
}

void print_all_elem_2(SparseMatrix *M)
{
	for(int i=0;i<=M->num_ele;++i)
	{
		printf("\n  %d %d %d",M->data[i].row,M->data[i].clo,M->data[i].ele);
	}
}



int main()
{
	//读取数据集合，获取用户数目
	int num_router=0;
	
	FILE* fp;
	const char* file_name="data.txt";
	
	if((fp=fopen(file_name,"r"))==NULL)
	{
		printf("\n Error:file not exits ");
		return -1;
	}
	
	const int line_length=1024;

	char* buffer=(char*)malloc(sizeof(char)*line_length);

	while(fgets(buffer,line_length,fp))
	{
		int i_1,i_2;
		sscanf(buffer,"%d %d",&i_1,&i_2);
		num_router=(num_router>i_1?num_router:i_1);
		num_router=(num_router>i_2?num_router:i_2);
	}
	fclose(fp);
	printf("\n usr_num: %ld",num_router);

	
	SparseMatrix* matrix=(SparseMatrix*)malloc(sizeof(SparseMatrix));
	inital_sparse_matrix(matrix,num_router,num_router);
	
	if((fp=fopen(file_name,"r"))==NULL)
	{
		printf(" \n Error: file open error");
		return -1;
	}

	while(fgets(buffer,line_length,fp))
	{
		int row,clo;
		sscanf(buffer,"%d %d",&row,&clo);
	//	printf("==================");
	//	printf("\n %d %d \n",row,clo);
		insert_elem(matrix,row,clo,1);
	//	print_all_elem_2(matrix);
	//	printf("\n ---------\n");
		insert_elem(matrix,clo,row,1);
	//	print_all_elem_2(matrix);
	//	int a;
	//	scanf("%d",&a);
	}
	fclose(fp);

	for(int i=1;i<=matrix->num_row;++i)
	{
		insert_elem(matrix,i,i,0);
	}

	printf("finish ...\n");
	//print_all_elem(matrix);
	
	free(buffer);
	free(matrix);
	return 0;


}
