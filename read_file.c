/*************************************************************************
	> File Name: read_file.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 14时36分04秒
 ************************************************************************/
// 这个只能支撑10000个用户 你先试试能不能用 不行的话 我等会发你一个用稀疏矩阵写的

#define LINE 1024
#define N 70000

#include<stdio.h>
#include<stdlib.h>

int main()
{
	const char* file_name="data.txt";
	FILE* fp;
	int raw,clu;
	

	if((fp=fopen(file_name,"r"))==NULL)
	{
		printf("file not exit!\n");
		return -1;
	}

	//申请缓冲区
	int **matrix;
	matrix=(int**)malloc(sizeof(int*)*N);
	for(int i=0;i<N;++i)
	{
		matrix[i]=(int*)malloc(sizeof(int)*N);
	}
	char* buffer=(char*)malloc(LINE*sizeof(char));
	
	
	for(int i=0;i<N;++i)
	{
		for(int j=0;j<N;++j)
		{
			matrix[i][j]=0;
		}
	}


	while(fgets(buffer,LINE,fp)!=NULL)
	{
		sscanf(buffer,"%d %d",&raw,&clu);
	//	printf("%d,%d\n",raw,clu);
		matrix[raw-1][clu-1]=1;
		matrix[clu-1][raw-1]=1;
	}

	free(buffer);
	fclose(fp);


	//算法主体
	//start-
	//end-

	//释放内存
	for(int i=0;i<N;++i)
	{
		free(matrix[i]);
	}
	free(matrix);

	
	return 0;
}
