/*************************************************************************
	> File Name: djistra.c
	> Author: Jangwee
	> Mail: jangwee11211039@gmail.com 
	> Created Time: 2014年04月22日 星期二 21时18分03秒
 ************************************************************************/

#include<stdio.h>
#define INF 65536
#define MAXSIZE 5 

int distance_of_index_point[MAXSIZE];
int previous_point_of_index_point[MAXSIZE];
bool flag[MAXSIZE];
void djistra(int graph[][MAXSIZE]);
void print_all_arrays();


void djistra(int graph[][MAXSIZE])
{
	for(int i=0;i<MAXSIZE;++i)
	{
		distance_of_index_point[i]=graph[0][i];
		previous_point_of_index_point[i]=0;
		flag[i]=true;
	}
	int k;
	for(int i=0;i<MAXSIZE;++i)
	{
		int min_dis=INF;
		for(int j=0;j<MAXSIZE;++j)
		{
			if((min_dis>distance_of_index_point[j])&&flag[j])
			{
				min_dis=distance_of_index_point[j];
				k=j;
			}
		}
		previous_point_of_index_point[i]=k;
		flag[k]=false;
		//更新
		for(int j=0;j<MAXSIZE;++j)
		{
			int new_distance=distance_of_index_point[k]+graph[k][j];
			if(new_distance<distance_of_index_point[j])
			{
				distance_of_index_point[j]=new_distance;
			}
		}
		//print_all_arrays();
		//int a;
		//scanf("\n %d  ",&a);
	}
}

void print_all_arrays()
{
	int i;
	for(i=0;i<MAXSIZE;++i)
	{
		printf("%d \t",distance_of_index_point[i]);
	}
	printf("\n");

	for(i=0;i<MAXSIZE;++i)
	{
		printf("%d \t",previous_point_of_index_point[i]);
	}
	printf("\n");
	for(i=0;i<MAXSIZE;++i)
	{
		printf("%d \t",flag[i]);
	}
	printf("\n");
}

int main()
{

	int graph[][MAXSIZE]={
		{0,10,INF,30,100},
		{10,0,50,INF,INF},
		{INF,50,0,20,10},
		{30,INF,20,0,60},
		{100,INF,10,60,0}
	};

	djistra(graph);
	print_all_arrays();
	return 0;
}
